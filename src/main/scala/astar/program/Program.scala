package astar.program

import java.nio.file.{Files, Paths}

import astar.algorithm._
import astar.model.{InputParsingError, _}
import cats.data.EitherT
import cats.effect.{ExitCode, IO}
import cats.implicits._

import scala.io.Source
import scala.util.Try

trait Program {
  type ParseInputResult = Either[InputParsingError, ParsedInput]
  def parseInput(args: List[String]): IO[ParseInputResult]

  def runAlgorithm(maze: Maze): AlgorithmResult

  def displayResults(result: AlgorithmResult): IO[ExitCode]

  def displayError(error: InputParsingError): IO[ExitCode]

  def displayError(ex: Throwable): IO[ExitCode]
}

object Program extends Program {
  implicit class StringToValidatedInt(s: String) {
    def parseAsInt(msgOnError: InputParsingError): Either[InputParsingError, Int] =
      Try(s.toInt).toEither.leftMap(_ => msgOnError)
  }

  override def  parseInput(args: List[String]): IO[ParseInputResult] = {

    def readNumberOfRows(lines: Lines): (Lines, Either[InputParsingError, Int]) = {
      readLineAsInt(lines, line => CouldNotParseNumberOfRows(line))
    }

    def readNumberOfColumns(lines: Lines): (Lines, Either[InputParsingError, Int]) =
      readLineAsInt(lines, line => CouldNotParseNumberOfColumns(line))

    def readLineAsInt(lines: Lines, handleError: String => InputParsingError): (Lines, Either[InputParsingError, Int]) = {
      val (linesLeft, lineE) = readLine(lines)
      (linesLeft, lineE.flatMap(line => line.parseAsInt(handleError(line))))
    }

    type Lines = List[String]
    def readLine(lines: Lines): (Lines, Either[InputParsingError, String]) =
      lines match {
        case Nil => (List.empty, NotValidFile.asLeft[String])
        case head :: tail => (tail, head.trim().asRight[InputParsingError])
      }

    def readLineAsListOfStrings(lines: Lines): (Lines, Either[InputParsingError, List[String]]) = {
      val (linesLeft, lineE) = readLine(lines)
      (linesLeft, lineE.map(_.split("""\s""").toList))
    }

    // The purpose of the line from below is to help Intellij compiler to properly recognize the type. Sbt has no problem without it.
    type ValidateCellsResult[A] = Either[InputParsingError, A]

    def validateCells(row: Int, cells: List[String]): Either[InputParsingError, List[Cell]] =
      cells.zipWithIndex.map {
        case ("s", column) => Start(column, row).asRight
        case ("g", column) => Destination(column, row).asRight
        case ("x", column) => Wall(column, row).asRight
        case ("_", column) => Empty(column, row).asRight
        case (unrecognizedCharacter, column) => UnrecognizedCharacter(unrecognizedCharacter, column).asLeft
      }.sequence[ValidateCellsResult, Cell]

    def readRowAsListOfCells(lines: Lines, row: Int): (Lines, Either[InputParsingError, Vector[Cell]]) = {
      val (linesLeft, cellsE) =  readLineAsListOfStrings(lines)
      (linesLeft, cellsE.flatMap(cells => validateCells(row, cells).map(_.toVector)))
    }

    def readNRows(lines: Lines, n: Int): Either[InputParsingError, Vector[Vector[Cell]]] = {
       (0 until n).toList.foldLeft((List.empty[Either[InputParsingError, Vector[Cell]]], lines)){
         case ((acc, linesLeft), row) =>
           val (linesLeft2, temporaryResult) = readRowAsListOfCells(linesLeft, row)
           (acc ++ List(temporaryResult), linesLeft2)
       }._1.sequence.map(_.toVector)
    }

    def readInputFile(args: List[String]): IO[Either[InputParsingError, List[String]]] = {
      args match {
        case List(path) =>
          IO(Files.exists(Paths.get(path))).map {
            case fileExists if fileExists == true => Source.fromFile(path).getLines().toList.asRight
            case _ => FileDoesNotExist(path).asLeft
          }
        case _ => IO(WrongNumberOfParameters(args.length).asLeft)
      }
    }

    (for {
      lines <- EitherT(readInputFile(args))

      (linesLeft, numOfRowsE) = readNumberOfRows(lines)
      numOfRows <- EitherT(IO(numOfRowsE))

      (linesLeft2, numOfColumnsE) = readNumberOfColumns(linesLeft)
      _ <- EitherT(IO(numOfColumnsE))

      rows <- EitherT(IO(readNRows(linesLeft2, numOfRows)))
      maze <- EitherT(IO(Maze.tryBuildMaze(rows)))
    } yield ParsedInput(maze)).value
  }

  override def runAlgorithm(maze: Maze): AlgorithmResult = {
    AStar.solve(maze) match {
      case None => NoSolution
      case Some(aStarState) =>
        AStar.reconstructPath(maze.destinationCell, aStarState)
          .map(Solution(_, aStarState.expansions, maze))
          .getOrElse(CouldNotReconstructPath)
    }
  }

  override def displayResults(result: AlgorithmResult): IO[ExitCode] = IO {
      val response = result match {
        case NoSolution => "Could not find or there is no solution for this maze."
        case CouldNotReconstructPath => "Could not reconstruct path (solution) for this maze."
        case solution: Solution => solution.toString()
      }

      println(response)
      ExitCode.Success
    }

  override def displayError(error: InputParsingError): IO[ExitCode] = IO {
    val response = error match  {
      case CouldNotParseNumberOfRows(line) =>
        s"$line <- is not valid number."

      case CouldNotParseNumberOfColumns(line) =>
        s"$line <- is not valid number."

      case UnrecognizedCharacter(unrecognizedCharacter, column) =>
        s"$unrecognizedCharacter <- from column: $column is not valid character of cell. Allowed characters are: [s, g, x,  _]."

      case NoDestinationCell=>
        "There is no destination Cell in given Maze."

      case NoStartingCell =>
        "There is no starting Cell in given Maze."

      case FileDoesNotExist(path: String) =>
        s"File: $path does not exist."

      case WrongNumberOfParameters(n: Int) =>
        s"Wrong number of parameters: $n. Program expects only one parameter (path to file)."

      case NotValidFile =>
        s"File contains data in not supported form."
    }

    println("\n########## ERROR ##########")
    println(response)
    ExitCode.Error
  }

  override def displayError(ex: Throwable): IO[ExitCode] = IO {
    println(s"Unexpected error: ${ex.getMessage}")
    ex.printStackTrace()
    ExitCode.Error
  }
}