package astar.model

import scala.math.abs

sealed trait Cell {
  def column: Int
  def row: Int

  private def rowDistanceTo(other: Cell): Int = abs(row - other.row)
  private def colDistanceTo(other: Cell): Int = abs(column - other.column)

  // Manhattan Distance
  def distanceTo(other: Cell): Int =
    rowDistanceTo(other) + colDistanceTo(other)
}

case class Destination(column: Int, row: Int) extends Cell {
  override def toString: String = "g"
}

case class Start(column: Int, row: Int) extends Cell {
  override def toString: String = "s"
}

case class Wall(column: Int, row: Int) extends Cell {
  override def toString: String = "x"
}

case class Empty(column: Int, row: Int) extends Cell {
  override def toString: String = "_"
}

case class Path(column: Int, row: Int) extends Cell {
  override def toString: String = "*"
}
