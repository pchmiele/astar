package astar.model

sealed trait InputParsingError

case class CouldNotParseNumberOfRows(line: String) extends InputParsingError

case class CouldNotParseNumberOfColumns(line: String) extends InputParsingError

case class UnrecognizedCharacter(character: String, column: Int) extends InputParsingError

case class FileDoesNotExist(path: String) extends InputParsingError

case class WrongNumberOfParameters(n: Int) extends InputParsingError

object NotValidFile extends InputParsingError

object NoStartingCell extends InputParsingError

object NoDestinationCell extends InputParsingError

case class ParsedInput(maze: Maze)
