package astar.model

import cats.implicits._

case class Maze(startingCell: Start, destinationCell: Destination, cells: Vector[Vector[Cell]]) {
  private def getValidCell(column: Int, row: Int): Option[Cell] = {
    cells.get(row).flatMap(_.get(column)).flatMap {
      case _: Wall => None
      case cell => Some(cell)
    }
  }

  def updateCell(originalCell: Cell, newCell: Cell): Maze = {
    originalCell match {
      case _: Destination => this
      case _: Start => this
      case _ =>
        cells.get(originalCell.row) match {
          case Some(row) => this.copy(cells = cells.updated(originalCell.row, row.updated(originalCell.column, newCell)))
          case None => this
        }
    }
  }

  def neighboursOf(cell: Cell): Set[Cell] =
    Seq(
      getValidCell(cell.column + 1, cell.row),
      getValidCell(cell.column - 1, cell.row),
      getValidCell(cell.column, cell.row + 1),
      getValidCell(cell.column, cell.row - 1)
    ).flatten.toSet

  override def toString(): String = {
    (for {
      rows <- cells
    } yield rows.map(_.toString).mkString(" ")).mkString("\n")
  }
}

object Maze {
  def tryBuildMaze(cellsMatrix: Vector[Vector[Cell]]): Either[InputParsingError, Maze] = {
    val cellsFlatten = cellsMatrix.flatten

    val destinationCellOpt = cellsFlatten.collect {
      case destination: Destination => destination
    }.headOption

    val startingCellOpt = cellsFlatten.collect {
      case start: Start => start
    }.headOption

    for {
      destinationCell <- Either.fromOption(destinationCellOpt, NoDestinationCell)
      startingCell <- Either.fromOption(startingCellOpt, NoStartingCell)
    } yield Maze(startingCell, destinationCell, cellsMatrix)
  }
}