package astar.model

sealed trait AlgorithmResult
object NoSolution extends AlgorithmResult
object CouldNotReconstructPath extends AlgorithmResult

case class Solution(cells: List[Cell], locationExpansions: Int, maze: Maze) extends AlgorithmResult {
    override def toString(): String = {
      val mazeWithPath = cells.foldLeft(maze)((acc, cell) => acc.updateCell(cell, Path(cell.column, cell.row)))

      s"""
         |########## Original Maze ##########
         |
         |${maze}
         |
         |########## Solution ##########
         |
         |$mazeWithPath
         |
         |Number of location expansion: $locationExpansions
       """.stripMargin
    }
}