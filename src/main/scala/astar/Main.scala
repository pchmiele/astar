package astar

import astar.model.ParsedInput
import astar.program.Program.{displayError, displayResults, parseInput, runAlgorithm}
import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._

object Main extends IOApp {

  def run(args: List[String]): IO[ExitCode] = {
    (for {
      parsedInput <- parseInput(args)
    } yield parsedInput match {
        case Right(ParsedInput(maze)) =>
          val solution = runAlgorithm(maze)
          displayResults(solution)

        case Left(error) => displayError(error)
      }
    ).flatten.handleErrorWith(displayError)
  }
}
