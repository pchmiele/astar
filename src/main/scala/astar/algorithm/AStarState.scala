package astar.algorithm

import astar.model._
import cats.Order
import dogs.Heap

case class AStarState
(
  closed: Set[Cell],
  open: Heap[Cell],
  gScores: Map[Cell, Double],
  fScores: Map[Cell, Double],
  cameFrom: Map[Cell, Cell],
  expansions: Int
) {

  implicit val ordering = new Order[Cell] {
    override def compare(x: Cell, y: Cell) = {
      val xFScore = fScores.getOrElse(x, Double.MaxValue)
      val yFScore = fScores.getOrElse(y, Double.MaxValue)

      val xGScore = gScores.getOrElse(x, Double.MaxValue)
      val yGScore = gScores.getOrElse(y, Double.MaxValue)

      if (xFScore < yFScore || (xFScore == yFScore && xGScore > yGScore)) -1 else 1
    }
  }

  def nextCell: Option[Cell] =
    open.getMin

  def removeCurrentCellFromOpen: AStarState =
    copy(open = open.remove)

  def addToClosed(cell: Cell): AStarState =
    copy(closed = closed + cell)

  def incrementNumberOfExpansions: AStarState =
    copy(expansions = expansions + 1)

  def updateScores(currentCell: Cell, neighbourCell: Cell, destinationCell: Cell): AStarState = {
    val currentGScore = gScores.getOrElse(currentCell, Double.MaxValue)
    val neighbourGScore = gScores.getOrElse(neighbourCell, Double.MaxValue)
    val tentativeGScore = currentGScore + 1

    if (tentativeGScore < neighbourGScore) {
      val heuristic = neighbourCell.distanceTo(destinationCell)
      copy(
        open = open.add(neighbourCell),
        gScores = gScores + (neighbourCell -> tentativeGScore),
        fScores = fScores + (neighbourCell -> (tentativeGScore + heuristic)),
        cameFrom = cameFrom + (neighbourCell -> currentCell)
      )
    } else {
      this
    }
  }
}

object AStarState {
  def initialState(maze: Maze): AStarState =
    AStarState(
      closed = Set.empty,
      open = Heap(maze.startingCell),
      gScores = Map(maze.startingCell -> 0),
      fScores = Map.empty,
      cameFrom = Map.empty,
      expansions = 0
    )
}
