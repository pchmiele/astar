package astar.algorithm

import astar.model.{Cell, _}

import scala.annotation.tailrec

object AStar {

  private def neighboursOfCellToVisit(neighboursOfCell: Set[Cell], alreadyVisitedCells: Set[Cell]): Set[Cell] =
    neighboursOfCell diff alreadyVisitedCells

  private def updateScores(neighbours: Set[Cell], state: AStarState, currentCell: Cell, maze: Maze): AStarState =
    neighbours
      .foldLeft(state){
        case (acc, neighbour) => acc.updateScores(currentCell, neighbour, maze.destinationCell)
      }
      .incrementNumberOfExpansions

  def solve(maze: Maze): Option[AStarState] = {
    @tailrec
    def loop(currentState: AStarState): Option[AStarState] = {
      currentState.nextCell match {
        case None => None
        case Some(_: Destination) => Some(currentState)
        case Some(currentCell) =>
          val updatedState = currentState
            .removeCurrentCellFromOpen
            .addToClosed(currentCell)

          val neighbours = neighboursOfCellToVisit(maze.neighboursOf(currentCell), updatedState.closed)
          val newState = updateScores(neighbours, updatedState, currentCell, maze)
          loop(newState)
      }
    }

    val initialState = AStarState.initialState(maze)
    loop(initialState)
  }

  def reconstructPath(destination: Destination, state: AStarState): Option[List[Cell]] = {
    @tailrec
    def loop(currentCell: Cell, path: List[Cell]): Option[List[Cell]] = {
      if(currentCell.isInstanceOf[Start]) {
        Some(path)
      } else {
        state.cameFrom.get(currentCell) match {
          case Some(previousCell) => loop(previousCell, previousCell :: path)
          case _ => None
        }
      }
    }

    loop(destination, List(destination))
  }
}