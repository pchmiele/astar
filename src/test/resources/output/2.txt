########## Original Maze ##########

x _ x x _
s _ _ x _
x x _ _ g

########## Solution ##########

x _ x x _
s * * x _
x x * * g

Number of location expansion: 6