########## Original Maze ##########

s _ _ _ _
_ _ _ _ _
_ _ _ _ _
_ _ _ _ g

########## Solution ##########

s * * * _
_ _ _ * *
_ _ _ _ *
_ _ _ _ g

Number of location expansion: 11