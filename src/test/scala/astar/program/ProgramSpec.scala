package astar.program

import java.io.{File, FileInputStream}

import astar.model._
import astar.utils.BaseSpec

class ProgramSpec extends BaseSpec {

  "Program.parseInput" should "return WrongNumberOfParameters when user provides wrong number of parameters" in {
    val params1 = List.empty
    Program.parseInput(List.empty).unsafeRunSync().left.value shouldBe WrongNumberOfParameters(params1.length)

    val params2 = List("firstArg", "secondArg")
    Program.parseInput(params2).unsafeRunSync().left.value shouldBe WrongNumberOfParameters(params2.length)
  }

  it should "return FileDoesNotExist when provided path points to file which does not exist" in new Context {
    val path = "pathToNotExistingFile"
    checkParseInput(path).left.value shouldBe FileDoesNotExist(path)
  }

  it should "return CouldNotParseNumberOfRows if different character than Int is used in first line" in new Context {
    val path = "/input/CouldNotParseNumberOfRows.txt"
    withResource(path)(checkParseInput).left.value shouldBe CouldNotParseNumberOfRows("a")
  }

  it should "return CouldNotParseNumberOfColumns if different character than Int is used in second line" in new Context {
    val path = "/input/CouldNotParseNumberOfColumns.txt"
    withResource(path)(checkParseInput).left.value shouldBe CouldNotParseNumberOfColumns("b")
  }

  it should "return NotValidFile when file is empty" in new Context {
    val path = "/input/EmptyFile.txt"
    withResource(path)(checkParseInput).left.value shouldBe NotValidFile
  }

  it should "return NoStartingCell when there is not starting cell" in new Context {
    val path = "/input/NoStartingCell.txt"
    withResource(path)(checkParseInput).left.value shouldBe NoStartingCell
  }

  it should "return NoDestinationCell when there is no destination cell" in new Context {
    val path = "/input/NoDestinationCell.txt"
    withResource(path)(checkParseInput).left.value shouldBe NoDestinationCell
  }

  it should "return UnrecognizedCharacter when there is no destination cell" in new Context {
    val path = "/input/UnrecognizedCharacter.txt"
    withResource(path)(checkParseInput).left.value shouldBe UnrecognizedCharacter("a", 0)
  }

  it should "return ParsedInput with valid Maze if input is in valid form" in new Context {
    val path = "/input/1.txt"
    withResource(path)(checkParseInput).right.value shouldBe ParsedInput(
      Maze(
        Start(0,1),
        Destination(1,0),
        Vector(
          Vector(Empty(0,0), Destination(1,0)),
          Vector(Start(0,1), Wall(1,1))
        )
      )
    )
  }

  trait Context {
    def withResource(name: String)(f: String => Program.ParseInputResult): Program.ParseInputResult = {
      val path = new File(getClass.getResource(name).getFile).getAbsolutePath
      f(path)
    }

    def checkParseInput(path: String): Program.ParseInputResult =
      Program.parseInput(List(path)).unsafeToFuture().futureValue
  }
}