package astar.program

import java.io.{ByteArrayOutputStream, File, FileInputStream}

import astar.Main
import astar.utils.BaseSpec
import cats.effect.ExitCode
import org.scalatest.Assertion

import scala.io.Source

class IntegrationSpec extends BaseSpec {

  "Maze 1" should "be solved correctly" in new Context {
    val input = "/input/1.txt"
    val output = "/output/1.txt"

    compare(input, output)
  }

  "Maze 2" should "be solved correctly" in new Context {
    val input = "/input/2.txt"
    val output = "/output/2.txt"

    compare(input, output)
  }

  "Maze 3" should "be solved correctly" in new Context {
    val input = "/input/3.txt"
    val output = "/output/3.txt"

    compare(input, output)
  }

  "Maze 4" should "be solved correctly" in new Context {
    val input = "/input/4.txt"
    val output = "/output/4.txt"

    compare(input, output)
  }

  "Maze 5" should "be solved correctly" in new Context {
    val input = "/input/5.txt"
    val output = "/output/5.txt"

    compare(input, output)
  }

  "Maze 6" should "be solved correctly" in new Context {
    val input = "/input/6.txt"
    val output = "/output/6.txt"

    compare(input, output)
  }

  "Maze 7" should "be solved correctly" in new Context {
    val input = "/input/7.txt"
    val output = "/output/7.txt"

    compare(input, output)
  }

  "Maze 8" should "be solved correctly" in new Context {
    val input = "/input/8.txt"
    val output = "/output/8.txt"

    compare(input, output)
  }

  "Maze 9" should "be solved correctly" in new Context {
    val input = "/input/9.txt"
    val output = "/output/9.txt"

    compare(input, output)
  }

  "Maze 10" should "be solved correctly" in new Context {
    val input = "/input/10.txt"
    val output = "/output/10.txt"

    compare(input, output)
  }

  trait Context {
    def compare(inputFileName: String, outputFileName: String): Assertion = {
      val inputPath = new File(getClass.getResource(inputFileName).getFile).getAbsolutePath
      val outputPath = Source.fromFile(getClass.getResource(outputFileName).getFile).mkString

      val outputStream = new ByteArrayOutputStream
      Console.withOut(outputStream) {
        Main.run(List(inputPath)).unsafeRunSync() shouldBe ExitCode.Success
        val result = outputStream.toString.trim.replace("\r", "").split("\n").toList
        val expectedResult = outputPath.trim.replace("\r", "").split("\n").toList
        result should contain theSameElementsAs expectedResult
      }
    }
  }
}