package astar.utils

import org.scalatest._
import org.scalatest.concurrent.ScalaFutures

abstract class BaseSpec extends FlatSpec with Matchers with OptionValues with EitherValues with ScalaFutures