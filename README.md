# Implementation of A* Algorithm

## Description

Implementation of A* Algorithm written in scala in pure functional way.
Program takes path to file with maze to solve as an argument, and prints result on Standard Output.

## Assumptions/Goals

1. To write program in pure functional way (no mutable state, pure functions, tail recursions, etc).
2. To present my knowledge about the functional data structures (immutable Set/List, Either, Option) and data types (EitherT, IO).
3. To offer good user experience by providing error handling for most cases, and to display results in a way that it is easy to understand.
4. To write e2e tests (take user input and compare the actual results with the expected ones)

## Prerequisites

In order to run the program you need to have [sbt](https://www.scala-sbt.org/) intalled.

## How to run

1. Go to project root directory
2. Run program:
```bash
sbt "run {path_to_input_file}"
``` 

Example:
```bash
sbt "run ./src/main/resources/input/10.txt"
```

## How to test

1. Run test
```bash
sbt test
```

## Results

You can view results for all example mazes. 
All results are under location ./src/test/resources/output

## Example output 

File: ./src/test/resources/output/9.txt

```bash
########## Original Maze ##########

_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
_ x _ _ _ _ _ _ x _ _ _ _ x _ _ _ _ _ _
_ _ x _ _ _ x _ _ _ _ _ _ x x x _ x _ _
_ _ _ x _ _ _ _ _ _ _ x _ _ _ _ _ _ _ _
_ _ _ _ _ _ x _ _ x _ _ x _ _ _ _ _ x _
_ _ _ _ x _ _ x _ _ _ _ _ x _ _ x _ _ _
_ x _ _ _ _ _ _ _ _ _ _ x _ _ _ _ _ _ _
_ _ s x _ _ x _ _ x _ _ _ _ x _ _ _ _ _
_ _ _ _ x _ _ _ x _ x _ _ _ _ x x _ _ _
_ _ _ _ _ _ _ _ _ _ _ x _ x _ _ _ x _ _
_ x _ _ _ _ x x _ _ _ _ _ _ _ x _ _ _ _
_ _ _ _ _ _ x x _ _ _ _ _ x x _ _ _ _ _
x _ x x _ x _ _ _ _ _ _ _ _ _ _ _ _ x _
_ x _ x x _ _ _ _ _ _ x x x _ _ _ _ x _
_ _ _ x _ _ _ _ _ _ _ _ _ _ _ _ x g _ _
_ x _ x _ _ _ _ _ _ _ x _ x _ _ _ _ _ _
_ _ _ _ _ _ x _ x _ _ _ _ _ _ _ _ x _ _
_ x _ x _ _ _ _ x _ x _ _ x _ _ x _ _ _
_ _ _ _ _ _ _ _ x _ _ _ _ _ _ _ _ _ _ _
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

########## Solution ##########

_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
_ x _ _ _ _ _ _ x _ _ _ _ x _ _ _ _ _ _
_ _ x _ _ _ x _ _ _ _ _ _ x x x _ x _ _
_ _ _ x _ _ _ _ _ _ _ x _ _ _ _ _ _ _ _
_ _ _ _ _ _ x _ _ x _ _ x _ _ _ _ _ x _
_ _ _ _ x _ _ x _ _ _ _ _ x _ _ x _ _ _
_ x _ _ _ _ _ _ _ _ _ _ x _ _ _ _ _ _ _
_ _ s x _ _ x _ _ x _ _ _ _ x _ _ _ _ _
_ _ * * x _ _ _ x _ x _ _ _ _ x x _ _ _
_ _ _ * * * * * * * * x _ x _ _ _ x _ _
_ x _ _ _ _ x x _ _ * * * _ _ x _ _ _ _
_ _ _ _ _ _ x x _ _ _ _ * x x _ _ _ _ _
x _ x x _ x _ _ _ _ _ _ * * * * * _ x _
_ x _ x x _ _ _ _ _ _ x x x _ _ * * x _
_ _ _ x _ _ _ _ _ _ _ _ _ _ _ _ x g _ _
_ x _ x _ _ _ _ _ _ _ x _ x _ _ _ _ _ _
_ _ _ _ _ _ x _ x _ _ _ _ _ _ _ _ x _ _
_ x _ x _ _ _ _ x _ x _ _ x _ _ x _ _ _
_ _ _ _ _ _ _ _ x _ _ _ _ _ _ _ _ _ _ _
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

Number of location expansion: 101
```

Where `*` is a path from start to goal.

## Area to improvement

There are two main areas where program could be improved: Program.parseInput function and AStar/AStarState. 
In both these places State monad could be used to get rid off passing the state to next function which expects is.

## Answers for questions 

1) The heuristic that you used.

[Manhatan distance](https://en.wiktionary.org/wiki/Manhattan_distance) was used to calculate heuristic.

2) What you used for tie-breakers when you had two nodes in your priority queue with the same priority.

In case of tie breakers I check the gScore for given two nodes (bigger the better). It means that algorithm prefers the paths which are longer.

3) What are the advantages of having a more sophisticated heuristic?  Are there any disadvantages?

If heuristic is admissable then the result of A* algorithm will always be the same (I mean the length of solution, not the particular path). 
The only difference will be the number of location expansion it will take to find the solution.

5) How do you know that a heuristic is admissable?  How do you know that a heuristic is monotonic?

Heuristic is admissable when it never overestimates the actual minimal cost of reaching the goal.
Heuristic is monotonic when it satisfy the following equation

```
h(x, g) <= distance(x, y) + h(y, g)

where:
    g - goal
    x, y - points
    h(x, g) - heuristic to get to the g from x
    h(y, g) - heuristic to get to the g from y
```

Our heuristic (Manhattan distance) is both admissable and monotonic.

6) Does the way you break ties matter?

Yes. The amount of nodes needed to check in order to find the solution depends on it.


