name := "astar"

version := "0.1"

scalaVersion := "2.12.4"

scalacOptions += "-Ypartial-unification"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % "1.1.0",
  "org.typelevel" %% "cats-effect" % "1.0.0-RC2",
  "org.typelevel" %% "dogs-core" % "0.6.10" exclude("org.typelevel", "cats-core_2.12"),
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
)